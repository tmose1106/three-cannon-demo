// rollup.config.js
import typescript from '@rollup/plugin-typescript';
import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  input: 'src/ts/app.ts',
  treeshake: true,
  output: {
    dir: 'src/js/',
    format: 'es',
    sourcemap: true,
  },
  plugins: [
    nodeResolve(),
    typescript({lib: ["es5", "es6", "dom"], target: "es6"})
  ],
  watch: {
    include: ['src/ts/*']
  }
};
