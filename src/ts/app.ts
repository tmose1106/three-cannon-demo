import * as THREE from 'three'
// import * as CANNON from 'cannon-es';

const scene = new THREE.Scene();

const geometry = new THREE.BoxGeometry();
const material = new THREE.MeshBasicMaterial({ color: 0x00ff00, wireframe: true });

const cube = new THREE.Mesh(geometry, material);
scene.add(cube);

const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

camera.position.z = 5;

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);

document.body.appendChild(renderer.domElement);

const rotx = Math.PI / 256,
    roty = Math.PI / 512;

async function animate() {
    requestAnimationFrame( animate );
    
    cube.rotateX(rotx);
    cube.rotateY(roty);

	renderer.render( scene, camera );
}

animate();
